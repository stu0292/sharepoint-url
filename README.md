# Sharepoint url converter

Takes a windows file path and turns it into a sharepoint url. The sharepoint url form depends on whether the path represents a folder or a file.

Syncing Sharepoint files to local machine is useful but sharing links with colleagues is cumbersome. This link takes a windows file path for a file synced from Sharepoint and turns it into a valid Sharepoint URL that can be accessed by users who already have access to the folder or file.

# Build

Build the executable from the source directory: `go build -o C:\\sharepoint-url.exe` (may require admin) or `go build` then copy the resulting executable to the `C:\` directory

## Add to Windows File Explorer context menu

Registry script to run this code from File Explorer context menu: add-to-context-menu.reg

Note this will add the context menu shortcut everywhere, including files outside of the Sharepoint sync directory. In these cases, the logic doesn't work so just ignore the context menu command.

TODO: improve the error message to suggest you're not in a sharepoint folder

TODO: add folder command to reg file: not sure which param to use https://superuser.com/a/473602

# Test

## Folder mode
go run main.go -folder "C:\Users\P123456\mycompany\The most amazeballs team - Documents\path\to\amazing\stuff"

## File mode
go run main.go "C:\Users\P123456\mycompany\The most amazeballs team - Documents\path\to\amazing\stuff"

# Troubleshooting

Copy down the input file path from Windows Explorer, the resulting sharepoint URL and what you think the sharepoint url should be (valid sharepoint to the same file if possible).

