// package main takes a windows file path and turns it into a sharepoint url. The sharepoint url form depends on whether the path represents a folder or a file
package main

import (
	"flag"
	"fmt"
	"github.com/atotto/clipboard"
	"net/url"
	"path"
	"regexp"
	"strings"
)

func main() {

	// Print errors all at the end (since the only output will be clipboard).

	// flag to specify file or folder mode
	bFolder := flag.Bool("folder", false, "If true, the filepath will be treated as a folder/directory path (sharepoint uses different url forms for files and folders)")
	bDownload := flag.Bool("download", false, "If true, the url will download the file immediately, rather than opening the file online")
	sSubsite := flag.String("subsite", "", "The parent of a subsite, gets prepended to the sitename: <domain>.sharepoint.com/<subsite>/<sitename>")
	flag.Parse()

	// path conversion
	sharepointUrl, err := ConvertPath(flag.Arg(0), *bFolder, *bDownload, *sSubsite)

	// output result (or error)
	if err != nil {
		output(err.Error())
	} else {
		output(sharepointUrl)
	}
}

// Extract the subdomain, sitename and item path from the input string. URL query escape the item's path, concatenate the result to one of the sharepoint url forms
func ConvertPath(filePath string, folder, download bool, subsite string) (urlPath string, err error) {

	// if filePath is empty, error
	if filePath == "" {
		return "", fmt.Errorf("Filepath cannot be empty")
	}

	// folder and download can both be false (ie file mode) but they can't both be true
	if folder && download {
		return "", fmt.Errorf("folder flag and download flag cannot both be true")
	}

	// extract the subdomain, sitename and item path from input string
	// windows path prefix (includes sharepoint subdomain and site name (minus spaces)):
	// C:\Users\<username>\<subdomain>\<site name with spaces> - Documents\<PATH>
	// "C:\Users\P123456\mycompany\The most amazeballs team - Documents\path\to\amazing\stuff"

	// regexp for expected filePath format
	re := regexp.MustCompile(`C:\\Users\\\w+\\(\w+)\\([^\\\/]+) - Documents\\(.+)`)

	// test filePath matches expected
	if ok := re.MatchString(filePath); !ok {
		return "", fmt.Errorf("Filepath has unrecognised format: %s", filePath)
	}

	// submatches are: subdomain, sitename (with spaces), path
	matches := re.FindStringSubmatch(filePath)
	subdomain := strings.ToLower(matches[1])
	// remove spaces from siteName
	siteName := strings.ReplaceAll(matches[2], " ", "")

	// prepend a subsite to sitename if provided if it's not present 
	if subsite != "" && !strings.Contains(siteName, subsite) {
	  siteName = path.Join(subsite, siteName)
	}

	// replace \ with / in path
	relPath := strings.ReplaceAll(matches[3], "\\", "/")
	// url escape the file path
	relPath = url.QueryEscape(relPath)
	// for some reason it doens't like + for space in the last path segment but wants %20, even though it redirects back to a path with + in??? + in the name gets sub'd for %2B
	relPath = strings.ReplaceAll(relPath, "+", "%20")

	// string template for files and folders
	if folder {
		// use folder prefix:
		// https://mycompany.sharepoint.com/sites/Themostamazeballsteam/Shared%20Documents%2F
		urlPath = fmt.Sprintf("https://%s.sharepoint.com/sites/%s/Shared%%20Documents%%2F%s", subdomain, siteName, relPath)

	} else if download {
		// use immediate download prefix:
		// https://mycompany.sharepoint.com/sites/Themostamazeballsteam/Shared%20Documents/path/to/amazing/stuff
		// immediate download url has slashes rather than %2F
		relPath = strings.ReplaceAll(relPath, "%2F", "/")

		// string template
		urlPath = fmt.Sprintf("https://%s.sharepoint.com/sites/%s/Shared%%20Documents/%s", subdomain, siteName, relPath)

	} else {
		// file prefix:
		// https://mycompany.sharepoint.com/:w:/r/sites/Themostamazeballsteam/_layouts/15/Doc.aspx?sourcedoc=%2Fsites%Themostamazeballsteam%2FShared%20Documents%2F
		urlPath = fmt.Sprintf("https://%s.sharepoint.com/:w:/r/sites/%s/_layouts/15/Doc.aspx?sourcedoc=%%2Fsites%%2F%s%%2FShared%%20Documents%%2F%s", subdomain, siteName, siteName, relPath)
	}

	return
}

// copy output to clipboard as well as stdout: github.com/atotto/clipboard
func output(s string) {
	// copy to clipboard
	if err := clipboard.WriteAll(s); err != nil {
		fmt.Println(err)
	}

	fmt.Println(s)
}
